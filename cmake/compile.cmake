
include(cmake/TargetCopyFiles.cmake)
add_custom_target(SolyMapCreatorModule ALL)
add_copy_directory(SolyMapCreatorModule ${CMAKE_SOURCE_DIR}
  RELATIVE Main/src/SolyMapCreator
  DESTINATION ${CMAKE_BINARY_DIR}/SolyMapCreator
  )
