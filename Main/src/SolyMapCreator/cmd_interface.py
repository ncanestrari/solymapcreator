
from backend import *

class CommandInterface(object):
    def __init__(self, project=None):
        super().__init__()
        self.project = project if project else Project()
        self.project_pm = [ func for func in dir(Project) if callable(getattr(Project, func)) and not func.startswith("__") ]
        self.map_pm = [ func for func in dir(Map) if callable(getattr(Map, func)) and not func.startswith("__") ]
        self.mission_pm = [ func for func in dir(Mission) if callable(getattr(Mission, func)) and not func.startswith("__") ]
        self.area_pm = [ func for func in dir(Areas) if callable(getattr(Areas, func)) and not func.startswith("__") ]
    
    def __parse_command__(self, command):
        if command[0] == 'Project':
            self.__parse_project_command__(command)
        elif command[0] == 'Map':
            self.__parse_map_command__(command)
        elif command[0] == 'Mission':
            self.__parse_mission_command__(command)
        elif command[0] == 'Areas':
            self.__parse_area_command__(command)
        else:
            pass
    
    def __parse_project_command__(self, command):
        if command[1] in self.project_pm:
            args = command[2::]
            self.project.__getattribute__(command[1])(*args)
    
    def __parse_map_command__(self, command):
        if command[1] in self.map_pm: 
            args = command[2::]
            self.project.mmap.__getattribute__(command[1])(*args)
    
    def __parse_mission_command__(self, command):
        if command[2] in self.mission_pm: 
            args = command[3::]
            self.project.__get_mission__(command[1]).__getattribute__(command[2])(*args)
    
    def __parse_area_command__(self, command):
        if command[1] in self.area_pm: 
            args = command[2::]
            self.project.mzon.__getattribute__(command[1])(*args)


    def parse(self, command):
        self.__parse_command__(command)

if __name__=='__main__':
    c = CommandInterface()

    cmd_list = [
        ('Map', 'add_landmark', 0.0, 0.0),
        ('Map', 'add_landmark', 0.0, 0.5),
        ('Map', 'add_landmark', 0.5, 0.0),
        ('Map', 'add_landmark', 0.5, 0.5),
        ('Project', 'add_mission', 'test_mission'),
        ('Mission', 'test_mission', 'add_task_hreach', '0 to 1', '0', '1', '1 to 2'),
        ('Mission', 'test_mission', 'add_task_hreach', '1 to 2', '1', '2', '2 to 3'),
        ('Mission', 'test_mission', 'add_task_hreach', '2 to 3', '2', '3', '3 to 0'),
        ('Mission', 'test_mission', 'add_task_hreach', '3 to 0', '3', '0', '0 to 1'),
        ('Areas', 'add_area_quietzone', 'quiet zone 1', [(0.25, 0.25),(-0.25, 0.25),(-0.25, -0.25), (0.25, -0.25)], 0.30, 0.8, 0.3, 0.8),
        ('Project', 'to_xml'),
        ('Project', 'save')
    ]

    for cmd in cmd_list:
        c.parse(cmd)

