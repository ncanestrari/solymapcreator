
import xmltodict

class Map(object):
    def __init__(self):
        super().__init__()
        self.landmarks = { 'Landmark' : [] }

    def add_landmark(self, posx, posy, orient=0., qrcode=u'auto', lmk_id=u'auto', next_ids=()):
        if qrcode == u'auto':
            qrcode = str(len(self.landmarks['Landmark']))
        if lmk_id == u'auto':
            lmk_id = qrcode
        self.landmarks['Landmark'].append({
                '@qrcode' : qrcode, 
                '@x' : posx, 
                '@y' : posy, 
                '@theta' : orient, 
                '@id' : lmk_id,
                '@next_ids' : u",".join( qr for qr in next_ids )})

    def landmark(self, lmk_id):
        lmks = { lmk['@id'] : lmk for lmk in self.landmarks['Landmark'] }
        return lmks[lmk_id]

    def to_xml(self):
        return xmltodict.unparse({ 'Map' : self.landmarks }, pretty=True, indent='  ')

    @staticmethod
    def from_xml(xml_str):
        m = Map()
        obj = dict(xmltodict.parse(xml_str))
        m.landmarks = obj['Map']
        return m

