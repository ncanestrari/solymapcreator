
import xmltodict

class Mission(object):
    def __init__(self, name='M001', mmap=None):
        super().__init__()
        self.tasks = { 
                '@name' : name,
                'HREACH' : [],
                'HTURN' : [],
                'GRAB' : [],
                'INJECT' : [],
                'CONDITION' : [],
                'IDLE' : [],
                }
        self.name = name
        self.set_map(mmap)

    def set_map(self, mmap):
        self.mmap = mmap
        self.rmap = { lmk['@id'] : lmk for lmk in self.mmap.landmarks['Landmark'] } if mmap else {}

    def add_task_hreach(self, name, lmk_id_from, lmk_id_to, next_task):
        lmk = self.rmap[lmk_id_to]
        self.tasks['HREACH'].append({
                '@name' : name, 
                '@lkm_id_from' : lmk_id_from,
                '@lmk_id_to' : lmk_id_to, 
                '@finalPosX' : lmk['@x'],
                '@finalPosY' : lmk['@y'],
                '@orient' : lmk['@theta'],
                '@next' : next_task })
        
    def add_task_hturn(self, name, lmk_id, next_task):
        self.tasks['HTURN'].append({
                '@name' : name, 
                '@lmk_id' : lmk_id, 
                '@orient' : lmk['theta'],
                '@next' : next_task })

    def add_task_grab(self, name, lmk_id, next_task):
        self.tasks['GRAB'].append({
                '@name' : name, 
                '@lmk_id' : lmk_id, 
                '@next' : next_task })

    def add_task_inject(self, name, lmk_id, next_task):
        self.tasks['INJECT'].append({
                '@name' : name, 
                '@lmk_id' : lmk_id, 
                '@next' : next_task })

    def add_task_condition(self, name, condition, next_task, else_task):
        self.tasks['CONDITION'].append({
                '@name' : name, 
                '@condition' : condition,
                '@next' : next_task, 
                '@otherwise' : else_task })

    def add_task_ask_SI(self):
        self.tasks['IDLE'].append({
                '@name' : 'AskSI' })
    
    def to_dict(self):
        return { 'Mission' : self.tasks }

    @staticmethod
    def from_dict(obj):
        m = Mission()
        m.name = obj['@name']
        m.tasks = obj
        return m

