
import os
import pickle
import xmltodict
from . import Project, Map, Mission, Areas

class Project(object):
    def __init__(self, name='Maps', mmap=Map(), mmis=[], mzon=Areas()):
        super().__init__()
        self.name = name
        self.mmis = mmis
        self.set_map(mmap)
        self.mzon = mzon

    def update(self):
        for v in self.mmis:
            v.set_map(self.mmap)

    def set_map(self, mmap):
        self.mmap = mmap
        self.update()
    
    def set_missions(self, missions):
        self.mmis = missions
        self.update()

    def add_mission(self, mission):
        if not isinstance(mission, Mission):
            mission = Mission(mission, self.mmap)
        else:
            mission.mmap = mmap
        self.mmis.append(mission)
    
    def set_areas(self, areas):
        self.mzon = areas

    def __mission_to_xml__(self):
        return xmltodict.unparse({'Missions' : [ m.to_dict() for m in self.mmis] }, pretty=True, indent='  ')

    def __get_mission__(self, misname):
        ms = { m.name : m for m in self.mmis }
        return ms[misname]

    def to_xml(self):
        if not os.path.exists(self.name):
            os.makedirs(self.name)

        print(self.mmap.to_xml(), file=open('{}/Map.xml'.format(self.name), 'w'))
        print(self.__mission_to_xml__(), file=open('{}/Missions.xml'.format(self.name), 'w'))
        print(self.mzon.to_xml(), file=open('{}/Areas.xml'.format(self.name), 'w'))

    @staticmethod
    def from_xml(folder):
        with open("{}/Map.xml".format(folder), 'r') as f:
            mmap = Map.from_xml(f.read())

        with open("{}/Missions.xml".format(folder), 'r') as f:
            obj = xmltodict.parse(f.read())
            mmis = [ Mission.from_dict(m) for m in obj['Missions'].values() ]

        with open("{}/Areas.xml".format(folder), 'r') as f:
            mzon = Areas.from_xml(f.read())
        
        p = Project(folder, mmap, mmis, mzon)
        return p

    def save(self, fname=None):
        fname = fname if fname else self.name
        pickle.dump(self, open("{}.prjt".format(fname), 'wb'))

    @staticmethod
    def load(fname):
        obj = pickle.load(open("{}.prjt".format(fname), 'rb'))
        obj.name = fname
        return obj

if __name__=="__main__":
    mmap = Map()
    mmap.add_landmark(0.0, 0.0)
    mmap.add_landmark(0.0, 0.5)
    mmap.add_landmark(0.5, 0.0)
    mmap.add_landmark(0.5, 0.5)

    mmis = Mission(u'Input', mmap)
    mmis.add_task_hreach(u'0 to 1', lmk_id_start=u'0', lmk_id_end=u'1', next_task=u'1 to 2')
    mmis.add_task_hreach(u'1 to 2', lmk_id_start=u'1', lmk_id_end=u'2', next_task=u'2 to 3')
    mmis.add_task_hreach(u'2 to 3', lmk_id_start=u'2', lmk_id_end=u'3', next_task=u'3 to 0')
    mmis.add_task_hreach(u'3 to 0', lmk_id_start=u'3', lmk_id_end=u'0', next_task=u'0 to 1')

    mzon = Areas()
    mzon.add_area_quietzone(u'quiet zone 1', [(0.25, 0.25),(-0.25, 0.25),(-0.25, -0.25), (0.25, -0.25)], 0.30, 0.8, 0.3, 0.8)

    prjt = Project(mmap=mmap, mmis=[mmis], mzon=mzon)
    prjt.to_xml()
    prjt.save()

    prjt2 = Project.from_xml('Maps')
    prjt2.name = 'Maps2'
    prjt2.to_xml()

    prjt3 = Project.load('Maps')
    prjt3.name = 'Maps3'
    prjt3.to_xml()
