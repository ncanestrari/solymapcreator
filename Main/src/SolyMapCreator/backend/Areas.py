
import xmltodict

class Areas(object):
    def __init__(self):
        super().__init__()
        self.areas = { 'QuiteZone' : [] }

    def add_area_quietzone(self, name, points, stop_dist, slow_dist, slow_speed, scan_dist):
        assert(len(points) > 3)
        self.areas['QuiteZone'].append({
                '@name'       : name,
                'Location'    : { 'Points' : [ {'@x' : x, '@y' : y} for x, y in points ] },
                '@stop_dist'  : stop_dist,
                '@slow_dist'  : slow_dist,
                '@slow_speed' : slow_speed,
                '@scan_dist'  : scan_dist})

    def to_xml(self):
        return xmltodict.unparse({ 'Areas' : self.areas }, pretty=True, indent='  ')

    @staticmethod
    def from_xml(xml_str):
        m = Areas() 
        obj = dict(xmltodict.parse(xml_str))
        m.areas = obj['Areas']
        return m

